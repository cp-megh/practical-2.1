package com.example.practical21

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.practical21.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {


    private lateinit var binding: ActivityMainBinding


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        var view = binding.root
        setContentView(view)

        createNotificationChannel()
        binding.btnTrigger.isEnabled = true
        binding.btnUpdate.isEnabled = false
        binding.btnCancel.isEnabled = false
        binding.btnTrigger.setOnClickListener { view -> triggerNotification() }
        binding.btnUpdate.setOnClickListener(View.OnClickListener { updateNotification() })
        binding.btnCancel.setOnClickListener(View.OnClickListener {
            val manager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            manager.cancel(resources.getInteger(R.integer.notificationId))
            binding.btnTrigger.isEnabled = true
            binding.btnUpdate.isEnabled = false
            binding.btnCancel.isEnabled = false
        })
    }

    private fun updateNotification() {
        val intent = Intent(this, NotificationDetailsActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, 0)
        val builder = NotificationCompat.Builder(this, getString(R.string.NEWS_CHANNEL_ID))
            .setSmallIcon(R.drawable.ic_notification)
            .setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.ic_icon_large))
            .setContentTitle(getString(R.string.title))
            .setContentText(getString(R.string.content_text))
            .setStyle(NotificationCompat.BigTextStyle().bigText(getString(R.string.content_text)))
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setContentIntent(pendingIntent)
            .setChannelId(getString(R.string.NEWS_CHANNEL_ID))
            .setAutoCancel(true)
        val notificationManagerCompat = NotificationManagerCompat.from(this)
        notificationManagerCompat.notify(
            resources.getInteger(R.integer.notificationId),
            builder.build()
        )
        binding.btnTrigger.isEnabled = false
        binding.btnUpdate.isEnabled = true
        binding.btnCancel.isEnabled = true
    }

    private fun triggerNotification() {
        val intent = Intent(this, NotificationDetailsActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, 0)
        val builder = NotificationCompat.Builder(this, getString(R.string.NEWS_CHANNEL_ID))
            .setSmallIcon(R.drawable.ic_notification)
            .setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.ic_icon_large))
            .setContentTitle(getString(R.string.title))
            .setContentText(getString(R.string.new_notification))
            .setStyle(
                NotificationCompat.BigTextStyle().bigText(getString(R.string.new_notification))
            )
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setContentIntent(pendingIntent)
            .setChannelId(getString(R.string.NEWS_CHANNEL_ID))
            .setAutoCancel(true)
        val notificationManagerCompat = NotificationManagerCompat.from(this)
        notificationManagerCompat.notify(
            resources.getInteger(R.integer.notificationId),
            builder.build()
        )
        binding.btnTrigger.isEnabled = false
        binding.btnUpdate.isEnabled = true
        binding.btnCancel.isEnabled = true
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel() {
        val sdk_int = Build.VERSION.SDK_INT
        val version_codes = Build.VERSION_CODES.O
        if (sdk_int >= version_codes) {
            val notificationChannel = NotificationChannel(
                getString(R.string.NEWS_CHANNEL_ID),
                getString(R.string.CHANNEL_NEWS),
                NotificationManager.IMPORTANCE_DEFAULT
            )
            notificationChannel.description = getString(R.string.CHANNEL_DESCRIPTION)
            notificationChannel.setShowBadge(true)
            val notificationManager = getSystemService(
                NotificationManager::class.java
            )
            notificationManager.createNotificationChannel(notificationChannel)
        }
    }

}